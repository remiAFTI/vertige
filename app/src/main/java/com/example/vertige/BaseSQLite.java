package com.example.vertige;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BaseSQLite extends SQLiteOpenHelper
{
    private static final String TABLE_VOIE = "table_voie";
    private static final String ID = "idV";
    private static final String RELAIS = "relaisV";
    private static final String COULEUR = "couleurV";
    private static final String DIFF = "diffV";
    private static final String TOP = "topV";
    private static final String DEGAINE = "degaineV";

    private static final String createBDD = "CREATE TABLE " + TABLE_VOIE + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + RELAIS + " TEXT NOT NULL, "
            + COULEUR + " TEXT NOT NULL, "
            + DIFF + " TEXT NOT NULL, "
            + TOP + " INTEGER DEFAULT 0, "
            + DEGAINE + " INTEGER DEFAULT 0);";

    public BaseSQLite (Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        //on créé la table à partir de la requête écrite dans la variable createBDD
        db.execSQL(createBDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }

    //cette méthode permet de récupérer toutes les donnée de la BDD
    public Cursor getData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_VOIE;
        Cursor data = db.rawQuery(query, null);

        return data;
    }


    public Cursor getItemId()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ID + " FROM " + TABLE_VOIE;
        Cursor data = db.rawQuery(query, null);

        return data;
    }

    public Cursor getNiveauMax()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT MAX(" + DIFF + ") FROM " + TABLE_VOIE + " WHERE " + TOP + " = 1";
        Cursor data = db.rawQuery(query, null);

        return data;
    }

    public Cursor getVoieEnCours()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_VOIE + " WHERE " + DEGAINE + " > 0  AND " + TOP + " = 0 ";
        Cursor data = db.rawQuery(query, null);

        return data;
    }

    public Cursor getVoieTop()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_VOIE + " WHERE " + TOP + " = 1 ";
        Cursor data = db.rawQuery(query, null);

        return data;
    }

    public Cursor getVoieNonGrimp()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_VOIE + " WHERE " + TOP + " = 0 AND " + DEGAINE + " = 0";
        Cursor data = db.rawQuery(query, null);

        return data;
    }


    public Cursor getItemCouleur(String relais)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COULEUR + " FROM " + TABLE_VOIE + " WHERE " + RELAIS + " = '" + relais + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getItemRelaisCouleur(String diff)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + RELAIS + " , " + COULEUR + " FROM " + TABLE_VOIE + " WHERE " + DIFF + " = '" + diff + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getItemRelaisDiff(String coul)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + RELAIS + " , " + DIFF + " FROM " + TABLE_VOIE + " WHERE " + COULEUR + " = '" + coul + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getCountVoie()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT COUNT(*) FROM " + TABLE_VOIE ;
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getCountVoieTop()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT COUNT(*) FROM " + TABLE_VOIE + " WHERE " + TOP + " = 1";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

}
