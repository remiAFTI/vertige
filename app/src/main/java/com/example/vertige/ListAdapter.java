package com.example.vertige;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ListAdapter extends ArrayAdapter<String>
{
    int vg;

    ArrayList<String> list;

    Context context;

    public ListAdapter(Context context, int vg, int id, ArrayList<String> list)
    {
        super(context, vg, id, list);

        this.context = context;

        this.vg = vg;

        this.list = list;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(vg, parent, false);

        TextView txtInfos = itemView.findViewById(R.id.txtInfos);

        txtInfos.setText(list.get(position));

        return itemView;

    }
}
