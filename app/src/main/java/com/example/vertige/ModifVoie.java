package com.example.vertige;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;

public class ModifVoie extends AppCompatActivity
{
    BaseSQLite maBaseSQLite;
    Voie uneVoie;
    VoieBDD voieBDD;

    EditText edtRelais, edtDiff, edtCoul, edtDegMax;
    Switch swtTop;
    Button btnSave, btnRetour;
    int selectId;
    String relaisBase, coulBase;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif_voie);

        edtRelais = findViewById(R.id.editTextRelais);
        edtDiff = findViewById(R.id.editTextDiff);
        edtCoul = findViewById(R.id.editTextCoul);
        edtDegMax = findViewById(R.id.editTextDegMax);
        swtTop = findViewById(R.id.switchTopModif);
        btnRetour = findViewById(R.id.buttonRetour);
        btnSave = findViewById(R.id.buttonSave);

        maBaseSQLite = new BaseSQLite(this, "voie.db", null, 1);
        voieBDD = new VoieBDD(this);
        uneVoie = new Voie();

        Intent receivedIntent = getIntent();
        selectId = receivedIntent.getIntExtra("idVoie", -1); //-1 est juste une valeur par default


        getData();

        relaisBase = edtRelais.getText().toString();
        coulBase = edtCoul.getText().toString();


        btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String relais = edtRelais.getText().toString();
                String diff = edtDiff.getText().toString();
                String coul = edtCoul.getText().toString();
                String degaine = edtDegMax.getText().toString();
                boolean etatTop = swtTop.isChecked();
                boolean voieExist = voieExist(relais, coul);

                if (TextUtils.isEmpty(relais))
                {
                    edtRelais.setError("Entrez un Relais !");
                }else
                    if (TextUtils.isEmpty(diff))
                    {
                        edtDiff.setError("Entrez une Difficulté !");
                    }else
                        if (TextUtils.isEmpty(coul))
                        {
                            edtCoul.setError("Entrez une Couleur !");
                        }else
                            if (TextUtils.isEmpty(degaine))
                            {
                                edtDegMax.setError("Entrez un Nombre de Dégaine !");
                            }else
                                if (voieExist == true)
                                {
                                    voieBDD.open();
                                    uneVoie = voieBDD.getVoieWithId(String.valueOf(selectId));

                                    uneVoie.setRelaisV(relais);
                                    uneVoie.setDiffV(diff);
                                    uneVoie.setCouleurV(coul);

                                    if(etatTop == true)
                                    {
                                        uneVoie.setTopV(1);
                                    }else
                                    {
                                        int nbrDeg = Integer.parseInt(degaine);
                                        uneVoie.setDegaineV(nbrDeg);
                                        uneVoie.setTopV(0);
                                    }

                                    voieBDD.updateVoie(uneVoie.getIdV(), uneVoie);

                                    Toast.makeText(getBaseContext(), "Modification Réussie", Toast.LENGTH_LONG).show();

                                    if (allVoieTop() == true)
                                    {
                                        bravo();
                                    }else
                                        {
                                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                            startActivity(intent);
                                        }
                                }
            }
        });

        btnRetour.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getBaseContext(), InfoVoie.class);
                intent.putExtra("id", Integer.parseInt(String.valueOf(selectId)));
                startActivity(intent);
            }
        });
    }

    private void getData()
    {
        voieBDD.open();
        uneVoie = voieBDD.getVoieWithId(String.valueOf(selectId));
        edtRelais.setText(uneVoie.getRelaisV());
        edtDiff.setText(uneVoie.getDiffV());
        edtCoul.setText(uneVoie.getCouleurV());
        edtDegMax.setText(String.valueOf(uneVoie.getDegaineV()));

        if(uneVoie.getTopV() == 1)
        {
            swtTop.setChecked(true);
        }
    }

    private boolean voieExist(String relais, String couleur)
    {
        boolean result;

        VoieBDD voieBDD = new VoieBDD(getBaseContext());
        voieBDD.open();

        Voie voieFromBdd = voieBDD.getVoieWithRelaisAndCouleur(relais, couleur);

        if (!relaisBase.equals(relais) && !coulBase.equals(couleur))
        {
            if (voieFromBdd != null)
            {
                result = false;
                Toast.makeText(getBaseContext(), "La voie " + voieFromBdd.getCouleurV() + " sur le Relais " + voieFromBdd.getRelaisV() + " existe déjà !", Toast.LENGTH_LONG).show();
            }
            else
            {
                result = true;
            }
        }else
            {
                result = true;
            }


        return result;
    }

    private boolean allVoieTop()
    {
        boolean result = false;
        int nbrVoieTop = 0;
        int nbrVoie = 0;
        Cursor countVoie = maBaseSQLite.getCountVoie();
        Cursor countVoieTop = maBaseSQLite.getCountVoieTop();


        while (countVoie.moveToNext())
        {
            nbrVoie = countVoie.getInt(0);
        }

        while (countVoieTop.moveToNext())
        {
            nbrVoieTop = countVoieTop.getInt(0);
        }

        if (nbrVoieTop == nbrVoie && nbrVoie != 0)
        {
            result = true;
        }

        return result;
    }

    private void bravo()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle("Félicitation !");
        builder.setMessage("Vous venez de réussir toutes les voies du mur. Pour vous récompensez, vous avez le droit de tout recommencer");
        builder.setPositiveButton("C'est Parti !!",
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(intent);
                    }
                });
        builder.setNegativeButton("C'est Tout !", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
                Toast.makeText(getBaseContext(), "OUI !", Toast.LENGTH_LONG).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
