package com.example.vertige;

public class Voie
{
    private int idV;
    private String relaisV;
    private String couleurV;
    private String diffV;
    private int topV;
    private int degaineV;

    public Voie() {}

    public Voie(String relaisV, String couleurV, String diffV, int topV, int degaineV)
    {
        this.relaisV = relaisV;
        this.couleurV = couleurV;
        this.diffV = diffV;
        this.topV = topV;
        this.degaineV = degaineV;
    }

    public Voie(String relaisV, String couleurV, String diffV)
    {
        this.relaisV = relaisV;
        this.couleurV = couleurV;
        this.diffV = diffV;
    }

    public int getIdV() {return idV; }

    public void setIdV(int idV) { this.idV = idV; }

    public String getRelaisV() { return relaisV; }

    public void setRelaisV(String relaisV) { this.relaisV = relaisV; }

    public String getCouleurV() { return couleurV; }

    public void setCouleurV(String couleurV) { this.couleurV = couleurV; }

    public String getDiffV() { return diffV; }

    public void setDiffV(String diffV) { this.diffV = diffV; }

    public int getTopV() { return topV; }

    public void setTopV(int topV) { this.topV = topV; }

    public int getDegaineV() { return degaineV; }

    public void setDegaineV(int degaineV) { this.degaineV = degaineV; }

    public String toString()
    {
        return "ID: " + idV + "\nRelais: " + relaisV + "\nCouleur: " + couleurV + "\nDiff: " + diffV + "\nTOP : " + topV + "\n Degaine: " + degaineV;
    }
}
