package com.example.vertige;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = "ListeVoie";

    BaseSQLite maBaseSQLite;
    ArrayList<String> listRelaisAjouter = new ArrayList<>();
    ArrayList<String> listDiffAjouter = new ArrayList<>();
    ArrayList<String> listCoulAjouter = new ArrayList<>();
    int idTri = 0;
    Button btnNewVoie, btnVoieRandom, btnMesVoie;
    RadioButton rbtnTriDiff, rbtnTriRelais, rbtnTriCoul;

    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnNewVoie = findViewById(R.id.buttonNewVoie);
        btnVoieRandom = findViewById(R.id.buttonVoieRandom);
        btnMesVoie = findViewById(R.id.buttonMesVoies);

        rbtnTriDiff = findViewById(R.id.radioButtonTriDiff);
        rbtnTriRelais = findViewById(R.id.radioButtonTriRelais);
        rbtnTriCoul = findViewById(R.id.radioButtonTriCoul);

        rbtnTriRelais.setChecked(true);


        mListView = findViewById(R.id.listv);

        maBaseSQLite = new BaseSQLite(this, "voie.db", null, 1);

        relaisListView();

        btnNewVoie.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getBaseContext(), NewVoie.class);
                startActivity(intent);
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String info = adapterView.getItemAtPosition(i).toString();

                Intent intent = new Intent(getBaseContext(), ListeInfo.class);

                if(idTri == 0)
                {
                    intent.putExtra("relaisS", info);

                }else
                    if (idTri == 1)
                    {
                        intent.putExtra("diffS", info);

                    }else
                        {
                            intent.putExtra("coulS", info);
                        }


                startActivity(intent);

            }
        });

        rbtnTriCoul.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                listCoulAjouter.clear();
                coulListView();

                rbtnTriCoul.setChecked(true);
                rbtnTriRelais.setChecked(false);
                rbtnTriDiff.setChecked(false);
            }
        });

        rbtnTriDiff.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                listDiffAjouter.clear();
                diffListView();

                rbtnTriDiff.setChecked(true);
                rbtnTriRelais.setChecked(false);
                rbtnTriCoul.setChecked(false);
            }
        });

        rbtnTriRelais.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                listRelaisAjouter.clear();
                relaisListView();

                rbtnTriRelais.setChecked(true);
                rbtnTriCoul.setChecked(false);
                rbtnTriDiff.setChecked(false);
            }
        });


        btnVoieRandom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getBaseContext(), RandomVoie.class);
                startActivity(intent);
            }
        });

        btnMesVoie.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getBaseContext(), MesVoies.class);
                startActivity(intent);
            }
        });

    }

    private void relaisListView()
    {
        Log.d(TAG, "Afficher les données !");

        //Obtenir les données et ajouter à une liste
        Cursor data = maBaseSQLite.getData();
        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            boolean b = relaisExistListe(data.getString(1));

            if ( b == false )
            {
                listRelaisAjouter.add(data.getString(1));

                //obtenir la valeur de la base de données dans la colonne 1 puis l'ajouter à ArrayList
                listData.add(data.getString(1));
            }

        }

        triList(listData);
        //Créer le ListAdapter et définir l'adaptateur
        ListAdapter adapter = new com.example.vertige.ListAdapter(this, R.layout.list_layout, R.id.txtInfos, listData);
        mListView.setAdapter(adapter);
        idTri = 0;

    }

    private boolean relaisExistListe(String relais)
    {
        boolean result = false;

        for (int i=0; i < listRelaisAjouter.size(); i++)
        {
            if(listRelaisAjouter.get(i).equals(relais))
            {
                result = true;
            }
        }

        return result;
    }

    private void diffListView()
    {
        Log.d(TAG, "Afficher les données !");

        //Obtenir les données et ajouter à une liste
        Cursor data = maBaseSQLite.getData();
        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            boolean b = diffExistListe(data.getString(3));

            if ( b == false )
            {
                listDiffAjouter.add(data.getString(3));

                //obtenir la valeur de la base de données dans la colonne 3 puis l'ajouter à ArrayList
                listData.add(data.getString(3));
            }

        }

        triList(listData);
        //Créer le ListAdapter et définir l'adaptateur
        ListAdapter adapter = new com.example.vertige.ListAdapter(this, R.layout.list_layout, R.id.txtInfos, listData);
        mListView.setAdapter(adapter);
        idTri = 1;

    }

    private boolean diffExistListe(String diff)
    {
        boolean result = false;

        for (int i=0; i < listDiffAjouter.size(); i++)
        {
            if(listDiffAjouter.get(i).equals(diff))
            {
                result = true;
            }
        }

        return result;
    }

    private void coulListView()
    {
        Log.d(TAG, "Afficher les données !");

        //Obtenir les données et ajouter à une liste
        Cursor data = maBaseSQLite.getData();
        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            boolean b = coulExistListe(data.getString(2));

            if ( b == false )
            {
                listCoulAjouter.add(data.getString(2));

                //obtenir la valeur de la base de données dans la colonne 2 puis l'ajouter à ArrayList
                listData.add(data.getString(2));
            }

        }

        triList(listData);
        //Créer le ListAdapter et définir l'adaptateur
        ListAdapter adapter = new com.example.vertige.ListAdapter(this, R.layout.list_layout, R.id.txtInfos, listData);
        mListView.setAdapter(adapter);
        idTri = 2;

    }

    private boolean coulExistListe(String coul)
    {
        boolean result = false;

        for (int i=0; i < listCoulAjouter.size(); i++)
        {
            if(listCoulAjouter.get(i).equals(coul))
            {
                result = true;
            }
        }

        return result;
    }


    private ArrayList<String> triList(ArrayList uneListe)
    {
        Collections.sort(uneListe);
        return uneListe;
    }
}
