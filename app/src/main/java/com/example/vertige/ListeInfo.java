package com.example.vertige;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListeInfo extends AppCompatActivity
{
    BaseSQLite maBaseSQLite;
    private ListView maListV;
    private String selectRelais, selectDiff, selectCoul;
    private int id;
    private TextView txtInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_info);

        maListV = findViewById(R.id.listvInfo);
        txtInfo = findViewById(R.id.textViewTitreInfo);

        maBaseSQLite = new BaseSQLite(this, "voie.db", null, 1);

        //Permet de récupérer les infos passer en supplément
        Intent receivedIntent = getIntent();
        selectRelais = receivedIntent.getStringExtra("relaisS");
        selectDiff = receivedIntent.getStringExtra("diffS");
        selectCoul = receivedIntent.getStringExtra("coulS");

        if (selectRelais != null)
        {
            couleurListView();
            System.out.println("///////////////////relai = " + selectRelais);
            txtInfo.setText(selectRelais);

        }else
            if (selectDiff != null)
            {
                RelaisCoulListView();
                txtInfo.setText(selectDiff);
            }else
                {
                    RelaisDiffListView();
                    txtInfo.setText(selectCoul);
                }

        maListV.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String lesInfos = adapterView.getItemAtPosition(i).toString();

                Intent intent = new Intent(getBaseContext(), InfoVoie.class);


                if (id == 0)
                {
                    String[] relDiff = lesInfos.split(" ");
                    intent.putExtra("relais", relDiff[0]);
                    intent.putExtra("couleur", selectCoul);
                }else
                    if (id == 1)
                    {
                        String[] relCoul = lesInfos.split(" ");
                        intent.putExtra("relais", relCoul[0]);
                        intent.putExtra("couleur", relCoul[1]);
                    }else
                        {
                            intent.putExtra("relais", selectRelais);
                            intent.putExtra("couleur", lesInfos);
                        }

                startActivity(intent);
            }
        });
    }

    private void RelaisDiffListView()
    {
        //Obtenir les données et ajouter à une liste
        Cursor data = maBaseSQLite.getItemRelaisDiff(selectCoul);
        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            //obtenir la valeur de la base de données dans la colonne 1 puis l'ajouter à ArrayList
            listData.add(data.getString(0) + " " + data.getString(1));
        }

        //Créer le ListAdapter et définir l'adaptateur
        ListAdapter adapter = new com.example.vertige.ListAdapter(this,R.layout.list_layout, R.id.txtInfos,listData);
        maListV.setAdapter(adapter);
        id = 0;
    }

    private void RelaisCoulListView()
    {
        Cursor data = maBaseSQLite.getItemRelaisCouleur(selectDiff);
        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            listData.add(data.getString(0) + " " + data.getString(1));
        }

        ListAdapter adapter = new com.example.vertige.ListAdapter(this,R.layout.list_layout, R.id.txtInfos,listData);
        maListV.setAdapter(adapter);
        id = 1;
    }

    private void couleurListView()
    {
        Cursor data = maBaseSQLite.getItemCouleur(selectRelais);
        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            listData.add(data.getString(0));
        }

        ListAdapter adapter = new com.example.vertige.ListAdapter(this, R.layout.list_layout, R.id.txtInfos, listData);
        maListV.setAdapter(adapter);
        id = 2;

    }
}
