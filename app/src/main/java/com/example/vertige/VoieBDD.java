package com.example.vertige;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class VoieBDD
{
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "voie.db";

    private static final String TABLE_VOIE = "table_voie";
    private static final String ID = "idV";
    private static final String RELAIS = "relaisV";
    private static final String COULEUR = "couleurV";
    private static final String DIFF = "diffV";
    private static final String TOP = "topV";
    private static final String DEGAINE = "degaineV";

    //ce sont des index
    private static final int numId = 0;
    private static final int unRelais = 1;
    private static final int uneCouleur = 2;
    private static final int uneDiff = 3;
    private static final int unTop = 4;
    private static final int uneDegaine = 5;

    private SQLiteDatabase bdd;
    private BaseSQLite maBaseSQLite;

    public VoieBDD(Context context)
    {
        //on créer la BDD et sa table
        maBaseSQLite = new BaseSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open()
    {
        //on ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close()
    {
        bdd.close();
    }

    public SQLiteDatabase getBDD()
    {
        return bdd;
    }

    public long insertVoie (Voie voie)
    {
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();

        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(RELAIS, voie.getRelaisV());
        values.put(COULEUR, voie.getCouleurV());
        values.put(DIFF, voie.getDiffV());

        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_VOIE, null, values);
    }

    public int updateVoie(int id, Voie voie)
    {
        //la mise à jour d'un compte dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simplement préciser quelle compte on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(RELAIS, voie.getRelaisV());
        values.put(COULEUR, voie.getCouleurV());
        values.put(DIFF, voie.getDiffV());
        values.put(TOP, voie.getTopV());
        values.put(DEGAINE, voie.getDegaineV());

        return bdd.update(TABLE_VOIE, values, ID + " = " + id, null);
    }

    public int removeVoieWithID(int id)
    {
        //suppresion d'un compte de la BDD grâce à l'ID
        return bdd.delete(TABLE_VOIE, ID + " = " + id, null);
    }

    public Voie getVoieWithId(String id)
    {
        //Récupere dans un Cursor les valeur correspondant à un compte contenu dans la BDD (ici avec le login)
        Cursor c = bdd.query(TABLE_VOIE, new String[] {ID, RELAIS, COULEUR, DIFF, TOP, DEGAINE},
                ID + " LIKE \"" + id +"\"" , null, null, null, null);


        return cursorToVoie(c);
    }

    public Voie getVoieWithRelaisAndCouleur(String relais, String couleur)
    {
        //Récupere dans un Cursor les valeurs correspondant à une voie contenu dans la BDD (ici avec le relais)
        Cursor c = bdd.query(TABLE_VOIE, new String[] {ID, RELAIS, COULEUR, DIFF, TOP, DEGAINE},
                RELAIS + " LIKE \"" + relais +"\" AND " + COULEUR + " LIKE \"" + couleur +"\""  , null, null, null, null);

        return cursorToVoie(c);
    }


    //cette méthode permet de convertir un cursor en un Compte
    private Voie cursorToVoie(Cursor c)
    {
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //sinon on se place sur le premier élément
        c.moveToFirst();

        //on créé un compte
        Voie voie = new Voie();

        //on lui affecte toutes les infos grâce aux infos contenues dans le cursor
        voie.setIdV(c.getInt(numId));
        voie.setRelaisV(c.getString(unRelais));
        voie.setCouleurV(c.getString(uneCouleur));
        voie.setDiffV(c.getString(uneDiff));
        voie.setTopV(c.getInt(unTop));
        voie.setDegaineV(c.getInt(uneDegaine));

        //on ferme le cursor
        c.close();

        //on retourne le compte
        return voie;

    }
}
