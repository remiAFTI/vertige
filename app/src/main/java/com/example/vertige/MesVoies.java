package com.example.vertige;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class MesVoies extends AppCompatActivity
{
    BaseSQLite maBaseSQLite;
    TextView txtNvxMax;
    RadioButton rbtnVoieTop, rbtnVoieCours, rbtnVoiesNonGrimper;

    private ListView maListView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mes_voies);

        txtNvxMax = findViewById(R.id.textViewNvxMax);
        rbtnVoieTop = findViewById(R.id.radioButtonVoieTop);
        rbtnVoiesNonGrimper = findViewById(R.id.radioButtonNonGrimper);
        rbtnVoieCours = findViewById(R.id.radioButtonVoieEnCours);
        rbtnVoieCours.setChecked(true);

        maListView = findViewById(R.id.listvMesVoies);

        maBaseSQLite = new BaseSQLite(this, "voie.db", null, 1);

        voieCoursListView();
        txtNvxMax.setText(niveauMax());

        maListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String lesInfos = adapterView.getItemAtPosition(i).toString();

                Intent intent = new Intent(getBaseContext(), InfoVoie.class);

                String[] relDiff = lesInfos.split(" ");
                intent.putExtra("relais", relDiff[0]);
                intent.putExtra("couleur", relDiff[1]);

                startActivity(intent);
            }
        });

        rbtnVoieTop.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                voieTopListView();
                rbtnVoieTop.setChecked(true);
                rbtnVoieCours.setChecked(false);
                rbtnVoiesNonGrimper.setChecked(false);
            }
        });

        rbtnVoieCours.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                voieCoursListView();
                rbtnVoieCours.setChecked(true);
                rbtnVoieTop.setChecked(false);
                rbtnVoiesNonGrimper.setChecked(false);

            }
        });

        rbtnVoiesNonGrimper.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                voieNonGrimpListView();
                rbtnVoiesNonGrimper.setChecked(true);
                rbtnVoieCours.setChecked(false);
                rbtnVoieTop.setChecked(false);

            }
        });
    }

    private void voieCoursListView()
    {
        //Obtenir les données et ajouter à une liste
        Cursor data = maBaseSQLite.getVoieEnCours();
        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            //obtenir la valeur de la base de données dans la colonne 1 puis l'ajouter à ArrayList
            listData.add(data.getString(1) + " " + data.getString(2) + " " + data.getString(3));
        }

        triList(listData);
        //Créer le ListAdapter et définir l'adaptateur
        ListAdapter adapter = new com.example.vertige.ListAdapter(this, R.layout.list_layout, R.id.txtInfos, listData);
        maListView.setAdapter(adapter);

    }

    private void voieTopListView()
    {
        //Obtenir les données et ajouter à une liste
        Cursor data = maBaseSQLite.getVoieTop();
        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            //obtenir la valeur de la base de données dans la colonne 1 puis l'ajouter à ArrayList
            listData.add(data.getString(1) + " " + data.getString(2) + " " + data.getString(3));
        }

        triList(listData);
        //Créer le ListAdapter et définir l'adaptateur
        ListAdapter adapter = new com.example.vertige.ListAdapter(this, R.layout.list_layout, R.id.txtInfos, listData);
        maListView.setAdapter(adapter);

    }

    private void voieNonGrimpListView()
    {
        //Obtenir les données et ajouter à une liste
        Cursor data = maBaseSQLite.getVoieNonGrimp();
        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            //obtenir la valeur de la base de données dans la colonne 1 puis l'ajouter à ArrayList
            listData.add(data.getString(1) + " " + data.getString(2) + " " + data.getString(3));
        }

        triList(listData);
        //Créer le ListAdapter et définir l'adaptateur
        ListAdapter adapter = new com.example.vertige.ListAdapter(this, R.layout.list_layout, R.id.txtInfos, listData);
        maListView.setAdapter(adapter);
    }

    private String niveauMax()
    {
        Cursor data = maBaseSQLite.getNiveauMax();
        String result = " ";

        ArrayList<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            listData.add(data.getString(0));
            result = data.getString(0);
        }



        return result;
    }

    private ArrayList<String> triList(ArrayList uneListe)
    {
        Collections.sort(uneListe);
        return uneListe;
    }
}
