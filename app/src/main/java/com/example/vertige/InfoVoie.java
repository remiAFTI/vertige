package com.example.vertige;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class InfoVoie extends AppCompatActivity
{
    private Button btnRetour, btnModif, btnSup;
    private TextView txtRelais, txtCoul, txtDiff, txtDegaine, txtDegMax;
    private CheckBox ckbxTop;

    BaseSQLite mBaseSQLite;
    Voie uneVoie;
    VoieBDD voieBDD;

    private String selectCoul, selectRelais;
    private int selectId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_voie);

        btnRetour = findViewById(R.id.buttonRetour);
        btnModif = findViewById(R.id.buttonModif);
        btnSup = findViewById(R.id.buttonSupp);

        txtRelais = findViewById(R.id.textViewRelais);
        txtCoul = findViewById(R.id.textViewCoul);
        txtDiff = findViewById(R.id.textViewDiff);
        txtDegaine = findViewById(R.id.textViewDegaine);
        txtDegMax = findViewById(R.id.textViewDegMax);

        ckbxTop = findViewById(R.id.checkBoxTop);
        ckbxTop.setEnabled(false);

        voieBDD = new VoieBDD(this);
        uneVoie = new Voie();
        mBaseSQLite = new BaseSQLite(this, "voie.db", null, 1);

        Intent receivedIntent = getIntent();
        selectRelais = receivedIntent.getStringExtra("relais");
        selectCoul = receivedIntent.getStringExtra("couleur");
        selectId = receivedIntent.getIntExtra("id", -1); //-1 est juste une valeur par default


        txtRelais.setText(selectRelais);
        txtCoul.setText(selectCoul);

        getData();


        btnRetour.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        btnSup.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                confirSupp();
            }
        });

        btnModif.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getBaseContext(), ModifVoie.class);
                intent.putExtra("idVoie", uneVoie.getIdV());
                startActivity(intent);
            }
        });
    }

    private void getData()
    {
        voieBDD.open();

        if (selectRelais == null && selectCoul == null)
        {
            uneVoie = voieBDD.getVoieWithId(Integer.toString(selectId));
            txtRelais.setText(uneVoie.getRelaisV());
            txtCoul.setText(uneVoie.getCouleurV());
            selectRelais = uneVoie.getRelaisV();

        }else
            {
                uneVoie = voieBDD.getVoieWithRelaisAndCouleur(selectRelais, selectCoul);
            }


        if (uneVoie != null)
        {
            txtDiff.setText(uneVoie.getDiffV());
            txtDegMax.setText(Integer.toString(uneVoie.getDegaineV()));

            if(uneVoie.getTopV() == 1)
            {
                ckbxTop.setChecked(true);
                txtDegaine.setVisibility(View.GONE);
                txtDegMax.setVisibility(View.GONE);
            }
        }
    }

    private void confirSupp()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle("Supprimer");
        builder.setMessage("Voulez-vous vraiment supprimer cette Voie ?");
        builder.setPositiveButton("OUI",
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        suppVoie();
                    }
                });
        builder.setNegativeButton("Surtout pas !", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void suppVoie()
    {
        voieBDD.open();

        uneVoie = voieBDD.getVoieWithRelaisAndCouleur(selectRelais, selectCoul);

        //s'il existe une voie possédant ce relais et couleur dans la BDD
        if (uneVoie != null)
        {
            //on supprime la voie de la BDD grâce a son id
            voieBDD.removeVoieWithID(uneVoie.getIdV());
            Toast.makeText(getBaseContext(), "Voie Supprimer", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
        }else
        {
            System.out.println("Ce compte n'existe pas");
        }
    }

}
