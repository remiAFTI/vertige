package com.example.vertige;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomVoie extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
    BaseSQLite maBaseSQLite;
    Button btnRandom;
    Spinner spinnerNvxMax;
    ArrayList<String> listDiffAjouter = new ArrayList<>();
    String[] laListDiff;
    String laDiffMax;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_voie);

        btnRandom = findViewById(R.id.buttonRandom);
        spinnerNvxMax = findViewById(R.id.spinnerNvxMax);
        spinnerNvxMax.setOnItemSelectedListener(this);

        maBaseSQLite = new BaseSQLite(this, "voie.db", null, 1);

        laListDiff = listeDiff();
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, laListDiff);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNvxMax.setAdapter(aa);

        btnRandom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Cursor data = maBaseSQLite.getItemId();
                ArrayList<Integer> listVoie = new ArrayList<>();
                boolean bool = false;
                int nbrRandom = 0;

                while (data.moveToNext())
                {
                    //obtenir la valeur de la base de données dans la colonne 0 puis l'ajouter à ArrayList
                    listVoie.add(Integer.parseInt(data.getString(0)));
                }


                while (bool == false)
                {
                    Random random = new Random();
                    nbrRandom = random.nextInt( listVoie.size() - 0 ) + 0;

                    bool = verifNvxMax(listVoie.get(nbrRandom));
                }

                Intent intent = new Intent(getBaseContext(), InfoVoie.class);

                intent.putExtra("id", listVoie.get(nbrRandom));
                startActivity(intent);
            }
        });

    }

    //Performing action onItemSelected and onNothing selected

    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id)
    {
        laDiffMax = laListDiff[position];
    }

    public void onNothingSelected(AdapterView<?> arg0)
    {
        // TODO Auto-generated method stub
    }

    private String[] listeDiff()
    {
        //Obtenir les données et ajouter à une liste
        Cursor data = maBaseSQLite.getData();
        List<String> listData = new ArrayList<>();

        while (data.moveToNext())
        {
            boolean b = diffExistListe(data.getString(3));

            if ( b == false )
            {
                listDiffAjouter.add(data.getString(3));

                //obtenir la valeur de la base de données dans la colonne 3 puis l'ajouter à ArrayList
                listData.add(data.getString(3));
            }

        }

        triList(listData);
        String[] listDiff = new String[ listData.size() ];
        listData.toArray(listDiff);


        return listDiff;

    }

    private boolean diffExistListe(String diff)
    {
        boolean result = false;

        for (int i=0; i < listDiffAjouter.size(); i++)
        {
            if(listDiffAjouter.get(i).equals(diff))
            {
                result = true;
            }
        }

        return result;
    }

    private List<String> triList(List<String> uneListe)
    {
        Collections.sort(uneListe);
        return uneListe;
    }


    private boolean verifNvxMax(int idVoie)
    {
        boolean result;
        VoieBDD voieBDD = new VoieBDD(this);
        voieBDD.open();
        Voie uneVoie = voieBDD.getVoieWithId(Integer.toString(idVoie));

        List<String> lesDiff = new ArrayList<>();
        lesDiff.add(uneVoie.getDiffV());
        lesDiff.add(laDiffMax);


        triList(lesDiff);


        if (lesDiff.get(0).equals(uneVoie.getDiffV()) || laDiffMax.equals(uneVoie.getDiffV()))
        {
            result = true;
        }else
            {
                result = false;
            }

        return result;
    }

}
