package com.example.vertige;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewVoie extends AppCompatActivity
{
    EditText etRelais, etCouleur, etDiff;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_voie);

        etRelais = findViewById(R.id.editTextRelais);
        etCouleur = findViewById(R.id.editTextCouleur);
        etDiff = findViewById(R.id.editTextDiff);

        Button btnAdd = findViewById(R.id.buttonAddVoie);

        btnAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                VoieBDD voieBDD = new VoieBDD(getBaseContext());

                String relais = etRelais.getText().toString();
                String couleur = etCouleur.getText().toString();
                String diff = etDiff.getText().toString();

                boolean etRemplie = editTextRemplie(relais, couleur, diff);
                boolean voieExist = voieExist(relais, couleur);

                if((etRemplie && voieExist) == true)
                {
                    Voie maVoie = new Voie(relais, couleur, diff);

                    voieBDD.open();

                    voieBDD.insertVoie(maVoie);

                    Toast.makeText(getBaseContext(), "Nouvelle Voie enregistrer !", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(intent);
                }
            }
        });

    }

    private boolean editTextRemplie (String relais, String couleur, String diff)
    {
        boolean result = false;

        if(!TextUtils.isEmpty(relais) && !TextUtils.isEmpty(couleur) && !TextUtils.isEmpty(diff))
        {
            result = true;
        }else
        {
            if (TextUtils.isEmpty(relais))
            {
                etRelais.setError("Entrez un Relais");
            }else
                {

                if (TextUtils.isEmpty(couleur))
                {
                    etCouleur.setError("Entrez une Couleur");
                } else etDiff.setError("Entrez une difficulté");
                }
        }

        return result;

    }

    private boolean voieExist(String relais, String couleur)
    {
        boolean result;

        VoieBDD voieBDD = new VoieBDD(getBaseContext());
        voieBDD.open();

        Voie voieFromBdd = voieBDD.getVoieWithRelaisAndCouleur(relais, couleur);

        if (voieFromBdd != null)
        {
            result = false;
            Toast.makeText(getBaseContext(), "La voie " + couleur + " sur le Relais " + relais + " existe déjà !", Toast.LENGTH_LONG).show();
        }
        else
            {
                result = true;
            }

        return result;
    }
}
